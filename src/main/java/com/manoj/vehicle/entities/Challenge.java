package com.manoj.vehicle.entities;

import java.util.HashMap;
import java.util.Map;


public class Challenge {
	
	public static void main(String[] args) {
	System.out.println(replaceConsonants("123456"));
	System.out.println(isVowel("a"));
	}
	
	public static String replaceConsonants(String s) {
		String[] split = s.split("");
		String result = "";
		Map<String, Integer>  dics = new HashMap<String, Integer>();
		dics.put("a", 1);
		dics.put("e", 2);
		dics.put("i", 3);
		dics.put("o", 4);
		dics.put("u", 5);
		String chrs = "abcdefghijklmnopqrstuvwxyz";
		String[] split2 = chrs.split("");;
		for (int i = 0;i<split.length;i++) {
			if(isVowel(split[i])) {
				result = result.concat(dics.get(split[i]).toString());
			}
			else if (split[i].equals("y")) {
				result = result.concat(" ");				
			}
			else if (split[i].equals(" ")) {
				result = result.concat("y");				
			}
			else if (chrs.contains(split[i])) {
				int index = chrs.indexOf(split[i]) -1;
				result = result.concat(split2[index]);
			}
			else if (isInteger(split[i])) {
				int num = Integer.parseInt(split[i]);
				int rev_num = 0; 
			    while (num > 0) 
			    {
			        rev_num = rev_num*10 + num%10; 
			        num = num/10; 
			    }				
				result = result + Integer.toString(rev_num);
			}
			else {
				result = result.concat(split[i]);
			}
		}
		return result;
		
	}

	public static boolean isVowel(String s) {
		if(s.equals("a") || s.equals("e")|| s.equals("i") || s.equals("o")  || s.equals("u")) {
			return true;
		} 
		return false;
	}
	
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
}
