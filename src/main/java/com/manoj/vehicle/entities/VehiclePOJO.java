package com.manoj.vehicle.entities;

import com.manoj.vehicle.enums.Make;
import com.manoj.vehicle.enums.Model;
import com.manoj.vehicle.enums.Status;

public class VehiclePOJO {
	private Make make;
	private Model model;
	private String vin;
	private Integer year;
	private double min;
	private double max;
	private Status status;
	
	public VehiclePOJO(Make make, Model model, String vin , Integer year, double min, double max, Status status) {
		super();
		this.make = make;
		this.model = model;
		this.vin = vin;
		this.year = year;
		this.min = min;
		this.max = max;
		this.status = status;
	}
	public Make getMake() {
		return make;
	}
	public void setMake(Make make) {
		this.make = make;
	}
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
