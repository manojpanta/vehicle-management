package com.manoj.vehicle.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manoj.vehicle.dao.VehicleDao;
import com.manoj.vehicle.entities.Vehicle;
import com.manoj.vehicle.entities.VehiclePOJO;
import com.manoj.vehicle.enums.Make;
import com.manoj.vehicle.enums.Status;

@Service
public class VehicleServiceImpl implements VehicleService {
	@Autowired
	private VehicleDao dao;
	

	@Override
	public Vehicle create(@Valid Vehicle vehicle) {
		return dao.save(vehicle);
	}

	@Override
	public Vehicle modify(@Valid Vehicle vehicle) {
		Optional<Vehicle> v = dao.findById(vehicle.getId());
		v.get().setMake(vehicle.getMake());
		v.get().setModel(vehicle.getModel());
		v.get().setVin(vehicle.getVin());;
		return dao.save(v.get());
	}

	@Override
	public Vehicle find(Long id) {
		return dao.findById(id).get();
	}

	@Override
	public void remove(Long id) {
		Optional<Vehicle> vehicle = dao.findById(id);
		dao.delete(vehicle.get());
	}

	@Override
	public List<Vehicle> findAll() {
		return dao.findAll();
	}

	@Override
	public List<Vehicle> findByVIN(String vin) {
		return dao.findByVin(vin);
	}

	@Override
	public Vehicle modifyPartially(@Valid Vehicle vehicle, Long id) {
		Optional<Vehicle> v = dao.findById(id);
		if (vehicle.getMake() != null) {
			v.get().setMake(vehicle.getMake());
		}
		if (vehicle.getModel() != null) {
			v.get().setModel(vehicle.getModel());
		}
		if (vehicle.getVin() != null) {
			v.get().setVin(vehicle.getVin());;
		}
		return dao.save(v.get());
	}
	
	@Override
	public Vehicle findByYear(int n) {
		return dao.findByYear(n);
	}

	@Override
	public List<Vehicle> findByMake(Make make) {
		return dao.findByMake(make);
	}

	@Override
	public List<Vehicle> refinedSearch(VehiclePOJO vehicle) {
		return dao.refinedSearch(vehicle);
	}

	@Override
	public List<Vehicle> findByPriceRange(Double min, Double max) {
		return dao.findByPriceBetween(min, max);
	}

	@Override
	public Object changeStatus(Long id, Status status) {
		Vehicle vehicle = find(id);
		if (vehicle.getStatus() == Status.Open) {
			switch(status) {
			case Cancelled:
				vehicle.setStatus(status);
				break;
			case Pending:
				vehicle.setStatus(status);
				break;
			case Sold:
				vehicle.setStatus(status);
				break;
			default:
				return defaultMessage("This vehicle can only be changed to Cancelled or Pending or Sold status");
			}
		} else if (vehicle.getStatus() == Status.Sold) {
			switch(status) {
			case Returned:
				vehicle.setStatus(status);
				break;
			default:
				return defaultMessage("This vehicle can only be changed to Returned status");
			}
		} else if (vehicle.getStatus() == Status.Cancelled) {
			switch(status) {
			case Pending:
				vehicle.setStatus(status);
				break;
			case Open:
				vehicle.setStatus(status);
				break;
			default:
				return defaultMessage("This vehicle can only be changed to Open and Pending status");
			}
		} else if (vehicle.getStatus() == Status.Returned) {
			switch(status) {
			case Pending:
				vehicle.setStatus(status);
				break;
			case Open:
				vehicle.setStatus(status);
				break;
			default:
				return defaultMessage("This vehicle can only be changed to Open and Pending status");
			}
		} else if (vehicle.getStatus() == Status.Pending) {
			switch(status) {
			case Open:
				vehicle.setStatus(status);
				break;
			case Cancelled:
				vehicle.setStatus(status);
				break;
			default:
				return defaultMessage("This vehicle can only be changed to Open and Cancelled status");
			}
		}
		return dao.save(vehicle);
	}
	
	
	public static Object defaultMessage(String message) {
		Map<String, String> obj = new HashMap<String, String>();
		obj.put("message", message);
		return obj;	
	}
}
