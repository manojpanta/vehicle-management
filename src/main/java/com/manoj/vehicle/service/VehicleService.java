package com.manoj.vehicle.service;

import java.util.List;
import java.util.Optional;

import com.manoj.vehicle.entities.Vehicle;
import com.manoj.vehicle.entities.VehiclePOJO;
import com.manoj.vehicle.enums.Make;
import com.manoj.vehicle.enums.Status;

public interface VehicleService {
	Vehicle create(final Vehicle vehicle);
	Vehicle modify(final Vehicle vehicle);
	Vehicle find(final Long id);
	void remove(final Long id);
	List<Vehicle> findAll();
	Vehicle modifyPartially(Vehicle vehicle, Long id);
	List<Vehicle> findByVIN(String vin);
	Vehicle findByYear(int n);
	List<Vehicle> findByMake(Make make);
	List<Vehicle> refinedSearch(VehiclePOJO vehicle);
	List<Vehicle> findByPriceRange(Double min, Double max);
	Object changeStatus(Long id, Status status);
}
