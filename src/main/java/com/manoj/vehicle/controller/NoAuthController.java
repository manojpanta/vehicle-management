package com.manoj.vehicle.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth/rest")
public class NoAuthController {
	
	@GetMapping("/hi")
	public String  sayHi() {
		return "Hi";	
	}
}
