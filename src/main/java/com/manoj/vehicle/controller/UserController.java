package com.manoj.vehicle.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/auth")
public class UserController {
	@GetMapping("/getmessage")
	public String greeting() {
		return "Manoj";
	}
}
