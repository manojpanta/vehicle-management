package com.manoj.vehicle.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.json.JsonParser;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.vehicle.entities.Vehicle;
import com.manoj.vehicle.entities.VehiclePOJO;
import com.manoj.vehicle.enums.Make;
import com.manoj.vehicle.enums.Model;
import com.manoj.vehicle.enums.Status;
import com.manoj.vehicle.service.VehicleService;
import com.manoj.vehicle.status.ChangeStatus;
import com.mysql.cj.xdevapi.JsonString;


@RestController
@RequestMapping("/vehicles")
public class VehicleController {
	
	@Autowired
	private VehicleService service;
	
	
	@PostMapping
	public Vehicle create(@RequestBody Vehicle vehicle) {
		return service.create(vehicle);
	}
	
	@GetMapping
	public List<Vehicle> findAll(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Vehicle find(@PathVariable Long id) {
		return service.find(id);
	}
	
	@PutMapping
	public Vehicle modify(@RequestBody Vehicle vehicle) {
		return service.modify(vehicle);
	}
	
	@PatchMapping("/{id}")
	public Vehicle modifyPartially(@RequestBody Vehicle vehicle, @PathVariable Long id) {
		return service.modifyPartially(vehicle, id);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		service.remove(id);
	}
	
	@GetMapping("/find")
	public List<Vehicle> findByVIN(@PathParam("vin") String vin) {
		return service.findByVIN(vin);		
	}
	
	@GetMapping("/find-by-year")
	public Vehicle findByYear(@PathParam("year") int year) {
		return service.findByYear(year);
	}
	
	@GetMapping("/find-by-make")
	public List<Vehicle> findByMake(@PathParam("make") Make make) {
		return service.findByMake(make);
	}
	
	@GetMapping("/find-by-price")
	public List<Vehicle> findByPriceRange(@PathParam("min") Double min, @PathParam("max") Double max) {
		return service.findByPriceRange(min, max);
	}
	
	@GetMapping("/refined-search")
	public List<Vehicle> refinedSearch(@PathParam("make") Make make,
									   @PathParam("model") Model model,
									   @PathParam("year") Integer year, 
									   @PathParam("status") Status status,
									   @PathParam("min") Double min,
									   @PathParam("max") Double max,
									   @PathParam("vin") String vin) {
		VehiclePOJO vehicle = new VehiclePOJO(make, model, vin, year, min, max, status);	
		return service.refinedSearch(vehicle);
	}
	
	@PostMapping("/change-status")
	public Object sellVehicle(@RequestBody ChangeStatus data) {
		return service.changeStatus(data.getId(), data.getStatus());
	}
}
