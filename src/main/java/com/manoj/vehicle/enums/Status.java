package com.manoj.vehicle.enums;

public enum Status {
	Open,
	Pending,
	Sold,
	Cancelled,
	Returned
}
