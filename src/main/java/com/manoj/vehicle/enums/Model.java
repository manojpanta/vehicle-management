package com.manoj.vehicle.enums;

public enum Model {
	Camry,
	Accord,
	Civic,
	Corolla,
	X2,
	RAV4

}
