package com.manoj.vehicle.enums;

public enum Make {
	Honda,
	Toyota,
	VolksWagen,
	Acura,
	BMW,
}
