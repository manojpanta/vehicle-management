package com.manoj.vehicle.status;

import com.manoj.vehicle.enums.Status;

public class ChangeStatus {
	private Long id;
	private Status status;
	public ChangeStatus(Long id, Status status) {
		super();
		this.id = id;
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
