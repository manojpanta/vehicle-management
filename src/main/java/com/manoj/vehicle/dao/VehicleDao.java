package com.manoj.vehicle.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.manoj.vehicle.entities.Vehicle;
import com.manoj.vehicle.entities.VehiclePOJO;
import com.manoj.vehicle.enums.Make;
@Repository
public interface VehicleDao extends JpaRepository<Vehicle, Long> {
    List<Vehicle> findByVin(String vin);
    Vehicle findByYear(Integer a);
	List<Vehicle> findByMake(Make make);	
	@Query("select v from Vehicle v "
			+ "where"
			+ " v.make = :#{#v1.getMake()} and"
			+ " v.model = :#{#v1.getModel()} and"
			+ " v.year = :#{#v1.getYear()} and"
			+ " v.status = :#{#v1.getStatus()} and" 
			+ " v.price between :#{#v1.getMin()} and :#{#v1.getMax()}")
	List<Vehicle> refinedSearch(VehiclePOJO v1);
	List<Vehicle> findByPriceBetween(Double min, Double max);
		
}
